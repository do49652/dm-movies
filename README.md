
# Movies
## Setup
Install Mongo
Install Node

Download and extract https://www.dropbox.com/s/l0foncqua4ipyoc/movies1.7z?dl=0

Fill the local database
```bash
mongorestore ./dump
```
  
Install dependencies
```bash
npm i
```

## Running dev
```bash
npm run dev
```
First run may have errors
  

## Running prod

```bash
npm run build
npm start
```