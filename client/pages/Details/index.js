import React, { useEffect, useState, useCallback, useMemo } from 'react'
import { AreaChart, Area, XAxis, Legend, Tooltip } from 'recharts'
import { useRouteMatch } from 'react-router-dom'
import getImagePalette from 'image-palette-core'
import { Link } from 'react-router-dom'
import { scaleTime } from 'd3-scale'
import Rating from 'react-rating'

import Flex from '../../components/Flex'
import Tabs from '../../components/Tabs'
import { formatCount } from '../../utils/formatCount'
import { LS_USER_KEY } from '../../App'
import { colors } from '../../styles'
import * as S from './styles'
import MediaType from '../../utils/mediaType'
import mediaType from '../../utils/mediaType'

const globalTimeMap = {}

function Cast({ cast }) {
  return (
    <S.Cast>
      {cast.slice(0, 6).map((c) => (
        <S.Actor key={c.id}>
          <div
            style={{ backgroundImage: `url(http://image.tmdb.org/t/p/w500${c.profile_path})` }}
          />
          <h3>{c.character}</h3>
          <h5>{c.name}</h5>
        </S.Actor>
      ))}
    </S.Cast>
  )
}

function SocialLinks({ homepage, external_ids }) {
  return (
    <S.SocialLinks>
      {homepage && (
        <a href={homepage} target="_blank">
          <img style={{ transform: 'scale(1.33)' }} src="/home.svg" />
        </a>
      )}
      {external_ids[0]['facebook_id'] && (
        <a href={`https://facebook.com/${external_ids[0]['facebook_id']}`} target="_blank">
          <img src="/facebook.svg" />
        </a>
      )}
      {external_ids[0]['instagram_id'] && (
        <a href={`https://instagram.com/${external_ids[0]['instagram_id']}`} target="_blank">
          <img src="/instagram.svg" />
        </a>
      )}
    </S.SocialLinks>
  )
}

function TrendsGraph({ trends, palette }) {
  if (!trends.length)
    return (
      <Flex justify="center" align="center">
        <h3 style={{ color: '#ccc', fontWeight: 400, fontStyle: 'italic' }}>
          No trends info for this movie.
        </h3>
      </Flex>
    )
  return (
    <AreaChart style={{ marginTop: 16 }} width={600} height={254} data={trends}>
      <defs>
        <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
          <stop offset="5%" stopColor={palette.color} stopOpacity={0.9} />
          <stop offset="100%" stopColor={palette.background} stopOpacity={0.3} />
        </linearGradient>
      </defs>
      <Legend
        verticalAlign="bottom"
        formatter={() => 'Movie popularity in google searches'}
        verticalAlign="top"
        height={36}
      />
      <Area
        type="monotone"
        dataKey="value"
        // stroke={palette.background}
        strokeWidth={0}
        // strokeOpacity={0.7}s
        fillOpacity={0.7}
        fill="url(#colorUv)"
      />
      <Tooltip labelFormatter={(time) => globalTimeMap[time]} />
      <XAxis
        scale={scaleTime().domain([trends[0].time, trends[trends.length - 1].time])}
        dataKey="time"
        hide
      />
    </AreaChart>
  )
}

function Ratings({ ratings }) {
  return (
    <>
      <S.RatingsAdditional>
        <div>
          <span>
            {ratings && ratings.twitter_followers
              ? `${ratings.twitter_followers}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
              : 'N/A'}
          </span>
          <S.RatingDescription>Twitter followers</S.RatingDescription>
        </div>

        <div>
          <span>{ratings && ratings.tmdb ? ratings.tmdb.vote_average : 'N/A'}</span>
          <S.RatingDescription>TMDB rating</S.RatingDescription>
        </div>

        <div>
          <span>
            {ratings && ratings.tmdb && ratings.tmdb.vote_count
              ? `${ratings.tmdb.vote_count}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
              : 'N/A'}
          </span>
          <S.RatingDescription>TMDB vote count</S.RatingDescription>
        </div>

        <div>
          <span>
            {ratings && ratings.youtube_vote_average
              ? Math.round(ratings.youtube_vote_average * 100) / 10
              : 'N/A'}{' '}
            (
            <span style={{ fontWeight: 400 }}>
              {ratings && ratings.youtube_views_count
                ? `${ratings.youtube_views_count}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                : 'N/A'}
            </span>
            )
          </span>
          <S.RatingDescription>Youtube vote average</S.RatingDescription>
        </div>
      </S.RatingsAdditional>

      <S.Ratings>
        <div>
          <span style={{ fontWeight: 400, fontSize: 36 }}>
            {(ratings && ratings.metascore) || 'N/A'}
          </span>
          <S.Vote percentage={(ratings && `${ratings.metascore}%`) || '0%'} />
          <S.RatingDescription>Metascore</S.RatingDescription>
        </div>

        <S.Separator />

        <div>
          <div style={{ marginBottom: 4 }}>
            <span style={{ fontWeight: 400, fontSize: 36 }}>
              {ratings ? ratings.imdb_vote_average : 'N/A'}
            </span>
          </div>
          <Rating
            readonly
            start={0}
            stop={10}
            initialRating={ratings ? ratings.imdb_vote_average : 0}
            fullSymbol={<img src="/star.svg" />}
            emptySymbol={<img src="/star_border.svg" />}
          />
          <S.RatingDescription>IMDB rating</S.RatingDescription>
        </div>

        <S.Separator />

        <div>
          <span style={{ fontWeight: 400, fontSize: 36 }}>
            {(ratings && ratings.rottenTomatoes) || 'N/A'}
          </span>
          <S.Vote percentage={(ratings && ratings.rottenTomatoes) || '0%'} />
          <S.RatingDescription>Rotten Tomatoes</S.RatingDescription>
        </div>
      </S.Ratings>
    </>
  )
}

function MovieDetails({ movie }) {
  const lableStyle = { fontWeight: 700, marginRight: 8 }
  const iconsStyles = {
    top: 3,
    display: 'inline',
    position: 'relative',
    marginRight: 4,
    height: 20,
  }

  return (
    <Flex justify="space-between">
      <div style={lableStyle}>
        <img style={iconsStyles} src="/date.svg" />
        {movie.release_date}
      </div>
      <div style={lableStyle}>
        <img style={iconsStyles} height={20} src="/money.svg" />
        {formatCount(movie.revenue)}
      </div>
      <div style={lableStyle}>
        <img style={iconsStyles} height={20} src="/time.svg" />
        {movie.runtime} Min
      </div>
    </Flex>
  )
}

function SimilarMovie({ movieId }) {
  const [movie, setMovie] = useState()

  useEffect(() => {
    window.scrollTo(0, 0)
    fetch(`/${MediaType.get()}/${movieId}`)
      .then((res) => res.json())
      .then(setMovie)
  }, [movieId])

  if (!movie || !movie.images_posters[0]) return null
  return (
    <S.MovieTile>
      <Link to={`/details/${movie.id}`}>
        <img
          src={`http://image.tmdb.org/t/p/w500${movie.images_posters[0]}`}
          onError="this.style.display='none'"
          // onError={`this.src="http://image.tmdb.org/t/p/w500${movie.images_posters[1]}}"`}
        />
      </Link>
      <h4 style={{ maxWidth: 200 }}>{movie.title}</h4>
    </S.MovieTile>
  )
}

function RelatedMovies({ movies }) {
  return (
    <>
      <S.SectionHeader>Similar movies</S.SectionHeader>
      <Flex
        justify="center"
        style={{ flexWrap: 'wrap', maxWidth: 980, margin: '0 auto', marginBottom: 32 }}
      >
        {movies.map((m) => (
          <SimilarMovie key={m} movieId={m} />
        ))}
      </Flex>
    </>
  )
}

function Details() {
  const match = useRouteMatch()
  const movieId = match.params.id
  const [movie, setMovie] = useState(null)
  const [trends, setTrends] = useState(null)
  const [imageURL, setImageURL] = useState()
  const [ratings, setRatings] = useState(null)
  const [user, setUser] = useState(
    localStorage.getItem(LS_USER_KEY) ? JSON.parse(localStorage.getItem(LS_USER_KEY)).user : null,
  )

  const [palette, setPalette] = useState({
    color: 'rgb(52, 54, 65)',
    backgroundColor: 'white',
    alternativeColor: 'rgb(119, 122, 134)',
  })

  // txtLight = rgb(247, 246, 249)

  useEffect(() => {
    fetch(`/${MediaType.get()}/${movieId}`)
      .then((res) => res.json())
      .then((m) => {
        setMovie(m)
        fetch(`/trends?keyword=${m.title}`)
          .then((res) => res.json())
          .then((_trends) => {
            setTrends(
              _trends.map(
                (t) => (
                  (globalTimeMap[t.time] = t.formattedAxisTime),
                  { value: t.value[0], time: Number(t.time) }
                ),
              ),
            )
          })
      })
  }, [movieId])

  useEffect(() => {
    const setImg = () => {
      const index = Math.round(Math.random() * Math.min(movie.images_posters.length, 6) - 1)
      let url = `http://image.tmdb.org/t/p/w500${movie.images_posters[index]}`

      fetch(url).then((res) => {
        const img = new Image()
        img.crossOrigin = 'anonymous'

        if (res.status >= 400) {
          url = `http://image.tmdb.org/t/p/w500${movie.images_posters[0]}`
        }

        img.src = url
        img.onload = () => setPalette(getImagePalette(img))
        setImageURL(url)
      })
    }

    if (movie && movie.external_ids && movie.external_ids[0].imdb_id) {
      fetch(`/ratings/${movie.external_ids[0].imdb_id}`)
        .then((res) => res.json())
        .then((_ratings) => {
          setRatings(_ratings)

          fetch(`/tmdb/${movie.id}`)
            .then((res) => res.json())
            .then((tmdb) => {
              setRatings({ ..._ratings, tmdb })

              let youtube = null
              let twitter = null

              try {
                fetch(`/youtube/${movie.videos[0]}`)
                  .then((res) => res.json())
                  .then((_youtube) => {
                    youtube = _youtube
                    setRatings({ ..._ratings, tmdb, ...twitter, ...youtube })
                  })
              } catch {}

              try {
                fetch(`/twitter/${movie.external_ids[0].twitter_id}`)
                  .then((res) => res.json())
                  .then((_twitter) => {
                    twitter = _twitter
                    setRatings({ ..._ratings, tmdb, ...youtube, ...twitter })
                  })
              } catch {}
            })
        })
    }

    if (movie) {
      setImg()
    }

    const intID = setInterval(setImg, 10000)
    return () => clearInterval(intID)
  }, [movie])

  const updateUser = useCallback(
    async (body) => {
      const res = await fetch('/user', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
      const _user = (await res.json()).user
      localStorage.setItem(LS_USER_KEY, JSON.stringify({ authorized: true, user: _user }))
      setUser(_user)
    },
    [user],
  )

  const toggleLike = useCallback(
    async (movieID) => {
      let likedMovies
      const key = `liked${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`;

      if (user[key].includes(movieID)) {
        const ind = user[key].indexOf(movieID)
        user[key].splice(ind, 1)
        likedMovies = [...user[key]]
      } else {
        likedMovies = [...user[key], movieID]
      }
      await updateUser({
        [key]: likedMovies,
      })
    },
    [user],
  )

  const toggleWatch = useCallback(
    async (movieID) => {
      let watchedMovies
      const key = `watched${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`;

      if (user[key].includes(movieID)) {
        const ind = user[key].indexOf(movieID)
        user[key].splice(ind, 1)
        watchedMovies = [...user[key]]
      } else {
        watchedMovies = [...user[key], movieID]
      }
      await updateUser({
        [key]: watchedMovies,
      })
    },
    [user],
  )

  const hasUserLikedMovie = useMemo((movieId) => {
    if (!user || !movie) return false
    const key = `liked${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`
    return user[key].includes(movie.id)
  }, [
    user, movie,
  ])

  const hasUserWatchedMovie = useMemo((movieId) => {
    if (!user || !movie) return false
    const key = `watched${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`
    return user[key].includes(movie.id)
  }, [
    user, movie,
  ])

  if (!movie) return null

  return (
    <>
      <S.DetailsPage>
        <S.GlobalStyle color={palette.color} />

        <Flex justify="space-around">
          <div style={{ marginRight: 48 }}>
            <S.Title style={{ color: 'black' }}>{movie.original_title || movie.title}</S.Title>
            <S.Genres>{movie.genres.slice(0, 3).join(' / ')}</S.Genres>
            {/* <S.Tagline>{movie.tagline}</S.Tagline> */}
            <Tabs style={{ minHeight: 520 }} activeColor={palette.color}>
              <Tabs.Tab title="Info">
                <Flex justify="space-between" align="center">
                  {movie.release_date ? <MovieDetails movie={movie} /> : <div />}
                  <SocialLinks homepage={movie.homepage} external_ids={movie.external_ids} />
                </Flex>
                <S.Overview>{movie.overview}</S.Overview>
                <Ratings ratings={ratings} />
              </Tabs.Tab>
              <Tabs.Tab title="Popularity">
                <div
                  style={{
                    width: 600,
                    maxWidth: '100%',
                    position: 'absolute',
                    minHeight: 220,
                    paddingTop: 32,
                  }}
                >
                  {trends && <TrendsGraph palette={palette} trends={trends} />}
                </div>
              </Tabs.Tab>
              <Tabs.Tab title="Videos">
                {movie.videos.slice(0, 4).map((v) => (
                  <iframe
                    key={v}
                    frameborder="0"
                    width={560 / 2}
                    allowFullScreen
                    height={315 / 2}
                    style={{ margin: 16, marginLeft: 0 }}
                    allow="autoplay; encrypted-media"
                    src={`https://www.youtube.com/embed/${v}`}
                  ></iframe>
                ))}
              </Tabs.Tab>
            </Tabs>
          </div>
          <div>
            <S.MoviePoster src={imageURL} />
            <Flex
              style={{ marginTop: 32, flexDirection: 'column' }}
              justify="center"
              align="center"
            >
              <S.MainButton
                style={{ background: palette.backgroundColor, color: palette.color }}
                done={hasUserLikedMovie}
                onClick={() => toggleLike(movie.id)}
              >
                {hasUserLikedMovie ? 'Liked' : 'Like'}
              </S.MainButton>
              <S.MainButton
                done={hasUserWatchedMovie}
                style={{ color: palette.backgroundColor, background: palette.alternativeColor }}
                onClick={() => toggleWatch(movie.id)}
                inverted
              >
                {hasUserWatchedMovie ? 'Watched' : 'Set watched'}
              </S.MainButton>
            </Flex>
          </div>
        </Flex>
      </S.DetailsPage>
      <Cast cast={movie.cast} />
      {(movie.similar_movies || movie.similar) && (
        <RelatedMovies movies={(movie.similar_movies || movie.similar).slice(0, 8)} />
      )}
    </>
  )
}

export default Details
