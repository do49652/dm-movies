import styled, { createGlobalStyle } from 'styled-components'
import { colors } from '../../styles'

export const GlobalStyle = createGlobalStyle`
  body {
    transition: .3s all;
    /* background: ${(props) => props.backgroundColor}; */
  }

  nav h3 a {
    color: ${(props) => props.color}!important;
  }
`

export const DetailsPage = styled.main`
  width: 1120px;
  padding-top: 64px;
  display: block;
  margin: 0 auto;
`

export const MoviePoster = styled.img`
  height: 500px;
  border-radius: 24px;
  max-width: 333.34px;
  min-width: 333.34px;
  transition: 0.3s all;
  box-shadow: 0 24px 38px 3px rgba(0, 0, 0, 0.14), 0 9px 46px 8px rgba(0, 0, 0, 0.12),
    0 11px 15px -7px rgba(0, 0, 0, 0.2);
`

export const Title = styled.h1`
  font-size: 56px;
  font-weight: 900;
  transition: 0.5s all;
  margin: 0;
`

export const Tagline = styled.h2`
  margin-top: 0;
  font-size: 16px;
  margin-bottom: 32px;
  font-family: 'IBM Plex Mono', monospace;
`

export const Overview = styled.p`
  color: #ccc;
  margin-right: 32px;
`

export const SocialLinks = styled.div`
  display: flex;
  font-size: 12px;
  font-weight: 400;
  align-items: center;
  justify-content: space-around;

  a {
    margin: 8px;
  }

  img {
    width: 20px;
    height: 20px;
  }
`

export const Vote = styled.div`
  height: 8px;
  width: 180px;
  margin: 11px 0;
  border-radius: 24px;
  background: whitesmoke;
  border: 1px solid black;

  &::after {
    content: '';
    height: 100%;
    display: block;
    background: black;
    position: relative;
    width: ${(p) => p.percentage};
  }
`

export const Cast = styled.div`
  margin-top: 40px;
  margin-bottom: 40px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  padding: 24px 16px;
  background: whitesmoke;
  box-sizing: border-box;
`

export const Actor = styled.div`
  width: 150px;

  div {
    width: 80px;
    height: 100px;
    background-repeat: no-repeat;
    background-position: top;
    background-size: cover;
    border-radius: 50%;
  }

  h3 {
    margin-bottom: 4px;
  }

  h5 {
    margin: 0;
    font-family: 'IBM Plex Mono', monospace;
  }
`

export const SectionHeader = styled.h1`
  margin: 32px;
  color: black;
  font-weight: 900;
`

export const MainButton = styled.button`
  width: 100%;
  border: none;
  font-size: 14px;
  cursor: pointer;
  font-weight: bold;
  padding: 8px 24px;
  display: block;
  border-radius: 24px;
  margin-bottom: 16px;
  transition: 0.5s all;
  color: ${(p) => (p.inverted ? (p.done ? 'black' : colors.PRIMARY) : 'white')};
  /* border: ${(p) => (p.inverted ? `1px solid ${p.done ? 'black' : colors.PRIMARY}` : 'none')}; */
  background: ${(p) => (p.inverted ? 'transparent' : p.done ? 'black' : colors.PRIMARY)};
  box-shadow: 0px 0px 2px -1px black;
`

export const Ratings = styled.div`
  width: 100%;
  display: flex;
  margin-top: 32px;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;

  > div {
    /* padding: 16px; */
  }
`

export const RatingsAdditional = styled.div`
  display: flex;
  justify-content: space-around;
  margin-top: 32px;
  padding: 16px;
  background: whitesmoke;
  border-radius: 12px;

  span {
    font-weight: 700;
    font-family: Lato;
    font-size: 18px;
  }
`

export const Separator = styled.div`
  width: 1px;
  height: 50px;
  background: grey;
`

export const Genres = styled.div`
  opacity: 0.7;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 24px;
  text-transform: uppercase;
  font-family: 'IBM Plex Mono', monospace;
`

export const RatingDescription = styled.div`
  font-family: 'IBM Plex Mono', monospace;
  font-size: 14px;
`

export const MovieTile = styled.div`
  flex: 1 1 25%;

  img {
    width: auto;
    border-radius: 24px;
    transition: 0.3s all;
    height: 300px;
    box-shadow: 0 24px 38px 3px rgba(0, 0, 0, 0.14), 0 9px 46px 8px rgba(0, 0, 0, 0.12),
      0 11px 15px -7px rgba(0, 0, 0, 0.2);

    &:hover {
      filter: grayscale(100%);
      box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12),
        0 2px 4px -1px rgba(0, 0, 0, 0.2);
    }
  }
`
