import React from 'react';
import { bind } from 'decko';
import MoviesPage from '../movies';
import getMediaType from '../../utils/getMediaType';

import './style.scss';

class HomePage extends React.Component {
  async componentDidUpdate() {
    if (this.fbFetch) {
      return;
    }

    const { user, setLikedMovies } = this.props;

    if (user) {
      user.recommendedMovies = await (await fetch('/recommender')).json();
      setTimeout(() => {
        this.forceUpdate();
      }, 2000);
    }

    if (user && user.fbAccessToken) {
      this.fbFetch = true;
      const moviesIds = await (await fetch(`/${getMediaType()}s/facebook`)).json();
      await setLikedMovies([...new Set(user.likedMovies.concat(moviesIds.map(({ id }) => id)))]);
    }

    this.forceUpdate();
  }

  @bind
  async updateMoviesList(property, moviesIds, limit = 10) {
    const { user } = this.props;
    if (!user || moviesIds === user[property]) return;

    const removeMovies = moviesIds;
    const addMovies = [];

    for (const movie of user[property]) {
      const i = removeMovies.indexOf(movie);
      if (i === -1) {
        addMovies.push(movie);
      } else {
        removeMovies.splice(i, 1);
      }
    }

    const res = await fetch(`/${getMediaType()}s`, {
      method: 'post',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify({
        ids: addMovies.slice(0, Math.min(limit, addMovies.length)),
      }),
    });

    return { addMovies: await res.json(), removeMovies };
  }

  render() {
    const { checkingLogin, user, selectMovie } = this.props;
    return (
      <div>
        {checkingLogin && <h1>...</h1>}
        {!checkingLogin && !user && <h1>You're not logged in</h1>}
        {!checkingLogin && user && (
          <>
            <h1>Hello {user.email}</h1>
            <div style={{ display: 'flex' }}>
              <div className="liked-movies">
                <h3>Movies you watched:</h3>
                <MoviesPage
                  key={user.watchedMovies.length}
                  height="calc(100vh - 200px)"
                  visible
                  customLoader={this.updateMoviesList.bind(null, 'watchedMovies')}
                  selectMovie={selectMovie}
                />
              </div>
              <div className="liked-movies">
                <h3>Movies you liked:</h3>
                <MoviesPage
                  key={user.likedMovies.length}
                  height="calc(100vh - 200px)"
                  visible
                  customLoader={this.updateMoviesList.bind(null, 'likedMovies')}
                  selectMovie={selectMovie}
                />
              </div>
              {user.recommendedMovies && (
                <div className="liked-movies">
                  <h3>Recommended movies:</h3>
                  <MoviesPage
                    key={user.recommendedMovies.length}
                    height="calc(100vh - 200px)"
                    visible
                    customLoader={this.updateMoviesList.bind(null, 'recommendedMovies')}
                    selectMovie={selectMovie}
                  />
                </div>
              )}
            </div>
          </>
        )}
      </div>
    );
  }
}

export default HomePage;
