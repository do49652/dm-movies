import React, { useEffect, useState } from 'react'
import Slider from 'react-slick'
import { Link } from 'react-router-dom'

import Flex from '../../components/Flex'
import ScrollToTop from '../../components/ScrollToTop'
import Loader from '../../components/Loader'

import * as S from '../Movies/styles'

const sliderConfig = {
  dots: true,
  speed: 2000,
  duration: 6000,
  autoplay: true,
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 3,
  variableWidth: true,
}

function MovieTile({ movie, isTrending }) {
  if (!movie.images_backdrops.length && isTrending) return null

  if (!movie.images_posters[0]) return null

  return (
    <S.MovieTileNoExpand isTrending={isTrending}>
      <S.PosterWrapper>
        <Link
          onClick={() => (
            localStorage.setItem('media', movie.similar_movies ? 'movie' : 'show'),
            (location.href = `/details/${movie.id}`)
          )}
        >
          <S.MoviePoster
            isTrending={isTrending}
            src={`http://image.tmdb.org/t/p/w500${
              (isTrending ? movie.images_backdrops : movie.images_posters)[0]
            }`}
          />
        </Link>
      </S.PosterWrapper>
      <S.MovieTitle>{movie.title}</S.MovieTitle>
    </S.MovieTileNoExpand>
  )
}

function usePopularMoviesRecommend(skip = 0, limit = 10) {
  const [popularMovieRecommendations, setPopularMovieRecommendations] = useState(null)

  useEffect(() => {
    fetch(`/recommend-popular-movies?skip=${skip}&limit=${limit}`)
      .then((res) => res.json())
      .then(setPopularMovieRecommendations)
  }, [skip, limit])

  return popularMovieRecommendations
}

function useRecommendByMovies(skip = 0, limit = 10) {
  const [movieRecommendations, setMovieRecommendations] = useState(null)

  useEffect(() => {
    fetch(`/recommend-movies?skip=${skip}&limit=${limit}`)
      .then((res) => res.json())
      .then(setMovieRecommendations)
  }, [skip, limit])

  return movieRecommendations
}

function usePopularShowRecommend(skip = 0, limit = 10) {
  const [popularShowRecommendations, setPopularShowRecommendations] = useState(null)

  useEffect(() => {
    fetch(`/recommend-popular-shows?skip=${skip}&limit=${limit}`)
      .then((res) => res.json())
      .then(setPopularShowRecommendations)
  }, [skip, limit])

  return popularShowRecommendations
}

function useRecommendByShows(skip = 0, limit = 10) {
  const [showRecommendations, setShowRecommendations] = useState(null)

  useEffect(() => {
    fetch(`/recommend-shows?skip=${skip}&limit=${limit}`)
      .then((res) => res.json())
      .then(setShowRecommendations)
  }, [skip, limit])

  return showRecommendations
}

function Recommender() {
  const mostPopularMovieRecommendations = usePopularMoviesRecommend()
  const recommendationsByMovies = useRecommendByMovies()
  const mostPopularShowRecommendations = usePopularShowRecommend()
  const recommendationsByShows = useRecommendByShows()

  return (
    <>
      <main style={{ margin: 16, overflow: 'hidden' }}>
        {(!mostPopularMovieRecommendations || !!(mostPopularMovieRecommendations || []).length) && (
          <S.ListHeader>Movies You might like</S.ListHeader>
        )}
        {mostPopularMovieRecommendations ? (
          !!(mostPopularMovieRecommendations || []).length && (
            <S.SliderRow isTrending>
              <Slider {...sliderConfig} slidesToShow={3} slidesToScroll={2}>
                {mostPopularMovieRecommendations.map((movie) => (
                  <MovieTile isTrending key={movie.id} movie={movie} />
                ))}
              </Slider>
            </S.SliderRow>
          )
        ) : (
          <Loader height={86} width={86} />
        )}

        {(!mostPopularShowRecommendations || !!(mostPopularShowRecommendations || []).length) && (
          <S.ListHeader>Shows You might like</S.ListHeader>
        )}
        {mostPopularShowRecommendations ? (
          !!(mostPopularShowRecommendations || []).length && (
            <S.SliderRow isTrending>
              <Slider {...sliderConfig} slidesToShow={3} slidesToScroll={2}>
                {mostPopularShowRecommendations.map((show) => (
                  <MovieTile isTrending key={show.id} movie={show} />
                ))}
              </Slider>
            </S.SliderRow>
          )
        ) : (
          <Loader height={86} width={86} />
        )}

        {(!recommendationsByMovies ||
          !!Object.keys(recommendationsByMovies.recommendations || []).length) && (
          <S.ListHeader>More recommendations to consider</S.ListHeader>
        )}
        {recommendationsByMovies ? (
          !!Object.keys(recommendationsByMovies || {}).length && (
            <div style={{ margin: '0 100px 0 40px', overflow: 'hidden', paddingLeft: 60 }}>
              {recommendationsByMovies.likedMovieDetails &&
                recommendationsByMovies.likedMovieDetails.map((likedMovie) => (
                  <S.SliderRow>
                    <Flex direction="row" justify="flex-start" align="center">
                      <div
                        style={{
                          height: '140%',
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'space-around',
                        }}
                      >
                        <h2
                          style={{ color: '#ccc', fontSize: 28, fontWeight: 400, marginBottom: 32 }}
                        >
                          Because you liked
                        </h2>
                        <MovieTile key={likedMovie.id} movie={likedMovie} />
                      </div>
                      <div>
                        <img
                          width={100}
                          src="/arrow_right.svg"
                          style={{ marginLeft: -30, opacity: 0.1 }}
                        />
                      </div>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          height: '140%',
                          overflow: 'auto',
                        }}
                      >
                        <h2
                          style={{ color: '#ccc', fontSize: 28, fontWeight: 400, marginBottom: 32 }}
                        >
                          You might like these too
                        </h2>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            height: '140%',
                            paddingLeft: 60,
                            overflow: 'auto',
                          }}
                        >
                          <div style={{ whiteSpace: 'nowrap', display: 'flex' }}>
                            {recommendationsByMovies.recommendations
                              .filter(({ id }) => likedMovie.id === id)
                              .map(({ movie }) => (
                                <div key={movie.id}>
                                  <MovieTile movie={movie} />
                                </div>
                              ))}
                          </div>
                        </div>
                      </div>
                    </Flex>
                  </S.SliderRow>
                ))}
            </div>
          )
        ) : (
          <Loader height={86} width={86} />
        )}

        {recommendationsByShows ? (
          !!Object.keys(recommendationsByShows || {}).length && (
            <div style={{ margin: '0 100px 0 40px', overflow: 'hidden', paddingLeft: 60 }}>
              {recommendationsByShows.likedShowDetails &&
                recommendationsByShows.likedShowDetails.map((likedShow) => (
                  <S.SliderRow>
                    <Flex direction="row" justify="flex-start" align="center">
                      <div style={{ display: 'flex', flexDirection: 'column', height: '140%' }}>
                        <h2
                          style={{ color: '#ccc', fontSize: 28, fontWeight: 400, marginBottom: 32 }}
                        >
                          Because you liked
                        </h2>
                        <MovieTile key={likedShow.id} movie={likedShow} />
                      </div>
                      <div>
                        <img
                          width={100}
                          src="/arrow_right.svg"
                          style={{ marginLeft: -30, opacity: 0.1 }}
                        />
                      </div>
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          height: '140%',
                          overflow: 'auto',
                        }}
                      >
                        <h2
                          style={{ color: '#ccc', fontSize: 28, fontWeight: 400, marginBottom: 32 }}
                        >
                          You might like these too
                        </h2>
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            height: '140%',
                            paddingLeft: 60,
                            overflow: 'auto',
                          }}
                        >
                          <div style={{ whiteSpace: 'nowrap', display: 'flex' }}>
                            {recommendationsByShows.recommendations
                              .filter(({ id }) => likedShow.id === id)
                              .map(({ show }) => (
                                <div key={show.id}>
                                  <MovieTile movie={show} />
                                </div>
                              ))}
                          </div>
                        </div>
                      </div>
                    </Flex>
                  </S.SliderRow>
                ))}
            </div>
          )
        ) : (
          <Loader height={86} width={86} />
        )}
      </main>
      <ScrollToTop />
    </>
  )
}

export default Recommender
