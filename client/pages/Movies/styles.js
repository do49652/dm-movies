import styled, { keyframes } from 'styled-components'

const fadeUp = keyframes`
   from {
    opacity: 0;
    filter: alpha(opacity=0);
    transform: translateY(20%);
  }
  to {
    opacity: 1;
    filter: alpha(opacity=100);
    transform: translateY(0);
  }
`

export const ListHeader = styled.h1`
  margin: 32px;
  color: black;
  font-weight: 900;
`

export const MovieDetails = styled.div`
  min-width: 120px;
  padding: 12px;
  height: ${(p) => (p.isTrending ? 190 : 276)}px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  > div {
    opacity: 0;
    animation: ${fadeUp} 0.2s ease-in-out 0.2s;
    animation-fill-mode: forwards;
  }

  > div + div {
    animation: ${fadeUp} 0.2s ease-in-out 0.3s;
    animation-fill-mode: forwards;
  }
`

export const MovieVote = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;
  font-family: 'IBM Plex Mono', monospace;

  img {
    height: 30px;
    width: auto;
    border-radius: 4px;
    margin-right: 8px;
  }
`

export const MovieTitle = styled.h4`
  max-width: 194px;
  white-space: normal;
`

export const MoviePoster = styled.img`
  width: auto;
  border-radius: 24px;
  transition: 0.3s all;
  height: ${(p) => (p.isTrending ? 220 : 300)}px;
  box-shadow: 0 24px 38px 3px rgba(0, 0, 0, 0.14), 0 9px 46px 8px rgba(0, 0, 0, 0.12),
    0 11px 15px -7px rgba(0, 0, 0, 0.2);
`

export const MovieTile = styled.div`
  width: ${(p) => (p.isTrending ? 470 : 280)}px;
  transition: 0.3s all;

  & ${MovieTitle} {
    ${(p) => p.isTrending && 'max-width: 280px;'}
  }

  &:hover {
    width: ${(p) => (p.isTrending ? 600 : 400)}px;

    ${MoviePoster} {
      filter: grayscale(100%);
      box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12),
        0 2px 4px -1px rgba(0, 0, 0, 0.2);
    }
  }
`

export const MovieTileNoExpand = styled.div`
  width: ${(p) => (p.isTrending ? 470 : 280)}px;
  transition: 0.3s all;

  &:hover {
    ${MoviePoster} {
      filter: grayscale(100%);
      box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12),
        0 2px 4px -1px rgba(0, 0, 0, 0.2);
    }
  }
`

export const PosterWrapper = styled.div`
  display: flex;
`

export const SliderRow = styled.div`
  min-height: ${(p) => (p.isTrending ? 343 : 430)}px;
`

export const Search = styled.div`
  display: flex;
  justify-content: center;
  margin: 68px 0;

  input {
    color: #333;
    height: 32px;
    width: 520px;
    border: none;
    outline: none;
    font-size: 16px;
    padding: 8px 24px;
    border-radius: 24px;
    background: rgba(0, 0, 0, 0.033);
  }
`

export const SocialLinks = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 16px;

  .facebook {
    background-image: url('/facebook.svg');
    background-repeat: no-repeat;
    display: block;
  }

  a {
    width: 24px;
    height: 24px;

    &:hover {
      img {
        display: none;
      }
      .hover {
        display: initial !important;
      }
    }

    img {
      width: 20px;
    }

    .hover {
      display: none !important;
    }
  }
`
