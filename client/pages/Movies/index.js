import React, { useEffect, useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import Slider from 'react-slick'

import Loader from '../../components/Loader'
import Flex from '../../components/Flex'
import ScrollToTop from '../../components/ScrollToTop'
import { formatCount } from '../../utils/formatCount'
import { LS_USER_KEY } from '../../App'

import * as S from './styles'

import MediaType from '../../utils/mediaType'
import mediaType from '../../utils/mediaType'

const sliderConfig = {
  dots: true,
  speed: 2000,
  duration: 10000,
  autoplay: true,
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 2,
  easing: 'ease-in-out',
  variableWidth: true,
}

function useMovies(skip = 0, limit = 10, query = '') {
  const [movies, setMovies] = useState([])

  useEffect(() => {
    fetch(`/${MediaType.get()}s?skip=${skip}&limit=${limit}&query=${query}`)
      .then((res) => res.json())
      .then(setMovies)
  }, [skip, limit, query])

  return movies.filter((m) => m.images_posters.length)
}

function MovieTile({
  movie,
  isTrending,
  toggleLike,
  toggleWatch,
  hasUserLikedMovie,
  hasUserWatchedMovie,
}) {
  const [isHover, setIsHover] = useState(false)
  const [ratings, setRatings] = useState(null)

  if (!movie.images_backdrops.length && isTrending) return null

  const onMouseEnter = () => {
    setIsHover(true)
    if (movie.external_ids && movie.external_ids[0].imdb_id) {
      fetch(`/ratings/${movie.external_ids[0].imdb_id}`)
        .then((res) => res.json())
        .then(setRatings)
    }
  }

  const onMouseLeave = () => {
    setIsHover(false)
    setRatings(null)
  }

  return (
    <S.MovieTile isTrending={isTrending} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
      <S.PosterWrapper>
        <Link to={`/details/${movie.id}`}>
          <S.MoviePoster
            isTrending={isTrending}
            src={`http://image.tmdb.org/t/p/w500${
              (isTrending ? movie.images_backdrops : movie.images_posters)[0]
            }`}
          />
        </Link>
        {isHover && (
          <S.MovieDetails isTrending={isTrending}>
            <div>
              {!ratings && <Loader height={86} width={86} />}
              {ratings && ratings.imdb_vote_average && (
                <S.MovieVote>
                  <img
                    style={{ height: 20 }}
                    src="https://pbs.twimg.com/profile_images/976507090624589824/0x28al44_bigger.jpg"
                  />
                  <div style={{ fontSize: 14 }}>
                    <div>
                      <span style={{ fontSize: 18, fontWeight: 700 }}>
                        {ratings.imdb_vote_average}
                      </span>
                      /10
                    </div>
                    <div style={{ fontSize: 14 }}>{formatCount(ratings.imdb_vote_count)} votes</div>
                  </div>
                </S.MovieVote>
              )}
              {ratings && ratings.rottenTomatoes && (
                <S.MovieVote>
                  <img
                    style={{ height: 20 }}
                    src=" https://www.rottentomatoes.com/assets/pizza-pie/images/icons/global/new-fresh-lg.12e316e31d2.png"
                  />
                  <div style={{ fontSize: 14 }}>
                    <div>
                      <span style={{ fontSize: 16, fontWeight: 700 }}>
                        {ratings.rottenTomatoes}
                      </span>
                    </div>
                  </div>
                </S.MovieVote>
              )}
              {movie.external_ids && movie.external_ids[0] && isTrending && (
                <S.SocialLinks>
                  {movie.external_ids[0]['facebook_id'] && (
                    <a
                      href={`https://facebook.com/${movie.external_ids[0]['facebook_id']}`}
                      target="_blank"
                    >
                      <img src="/facebook.svg" />
                      <img className="hover" src="/facebook(1).svg" />
                    </a>
                  )}
                  {movie.external_ids[0]['instagram_id'] && (
                    <>
                      {movie.external_ids[0]['facebook_id'] && (
                        <div style={{ height: 24, width: 1, background: 'black' }} />
                      )}
                      <a
                        href={`https://instagram.com/${movie.external_ids[0]['instagram_id']}`}
                        target="_blank"
                      >
                        <img src="/instagram.svg" />
                        <img className="hover" src="/instagram(1).svg" />
                      </a>
                    </>
                  )}
                  {movie.external_ids[0]['twitter_id'] && (
                    <>
                      {movie.external_ids[0]['instagram_id'] && (
                        <div style={{ height: 24, width: 1, background: 'black' }} />
                      )}
                      <a
                        href={`https://twitter.com/${movie.external_ids[0]['twitter_id']}`}
                        target="_blank"
                      >
                        <img src="/twitter.svg" />
                        <img className="hover" src="/twitter(1).svg" />
                      </a>
                    </>
                  )}
                </S.SocialLinks>
              )}
            </div>
            {!isTrending && (
              <Flex justify="space-between" style={{ flexDirection: 'column' }}>
                <a
                  style={{ cursor: 'pointer', marginBottom: 8 }}
                  onClick={() => toggleLike(movie.id)}
                >
                  {hasUserLikedMovie(movie.id) ? (
                    <>
                      <img height={25} src="/like-fill.svg" />
                      <div style={{ fontFamily: "'IBM Plex Mono', monospace", fontSize: 14 }}>
                        Liked
                      </div>
                    </>
                  ) : (
                    <>
                      <img height={25} src="/like.svg" />
                      <div style={{ fontFamily: "'IBM Plex Mono', monospace", fontSize: 14 }}>
                        Like
                      </div>
                    </>
                  )}
                </a>
                <a
                  style={{
                    cursor: 'pointer',
                  }}
                  onClick={() => toggleWatch(movie.id)}
                >
                  {hasUserWatchedMovie(movie.id) ? (
                    <>
                      <img height={25} src="/done.svg" />
                      <div style={{ fontFamily: "'IBM Plex Mono', monospace", fontSize: 14 }}>
                        Watched
                      </div>
                    </>
                  ) : (
                    <>
                      <img height={25} src="/watch_later.svg" />
                      <div style={{ fontFamily: "'IBM Plex Mono', monospace", fontSize: 14 }}>
                        Set watched
                      </div>
                    </>
                  )}
                </a>
              </Flex>
            )}
          </S.MovieDetails>
        )}
      </S.PosterWrapper>
      <S.MovieTitle>{movie.title}</S.MovieTitle>
    </S.MovieTile>
  )
}

function Movies() {
  const [movieQuery, setMovieQuery] = useState('')

  const trending = useMovies()
  const top = useMovies(20)
  const _rest = useMovies(movieQuery ? 0 : 40, 100, movieQuery)

  const rest = []
  const showTopSections = true // movieQuery === ''

  const [user, setUser] = useState(
    localStorage.getItem(LS_USER_KEY) ? JSON.parse(localStorage.getItem(LS_USER_KEY)).user : null,
  )

  useEffect(() => {
    ;(async () => {
      try {
        const res = await fetch('/authorized')
        const _auth = await res.json()
        setUser(_auth.user)
        if (_auth.authorized) {
          localStorage.setItem(LS_USER_KEY, JSON.stringify(_auth))
        } else {
          localStorage.removeItem(LS_USER_KEY)
        }
      } catch (e) {
        localStorage.removeItem(LS_USER_KEY)
      }
    })()
  }, [])

  const updateUser = useCallback(
    async (body) => {
      const res = await fetch('/user', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
      })
      const _user = (await res.json()).user
      localStorage.setItem(LS_USER_KEY, JSON.stringify({ authorized: true, user: _user }))
      setUser(_user)
    },
    [user],
  )

  const toggleLike = useCallback(
    async (movieID) => {
      let likedMovies
      const key = `liked${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`

      if (user[key].includes(movieID)) {
        const ind = user[key].indexOf(movieID)
        user[key].splice(ind, 1)
        likedMovies = [...user[key]]
      } else {
        likedMovies = [...user[key], movieID]
      }
      await updateUser({
        [key]: likedMovies,
      })
    },
    [user],
  )

  const toggleWatch = useCallback(
    async (movieID) => {
      let watchedMovies
      const key = `watched${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`

      if (user[key].includes(movieID)) {
        const ind = user[key].indexOf(movieID)
        user[key].splice(ind, 1)
        watchedMovies = [...user[key]]
      } else {
        watchedMovies = [...user[key], movieID]
      }
      await updateUser({
        [key]: watchedMovies,
      })
    },
    [user],
  )

  const hasUserLikedMovie = useCallback(
    (movieId) => {
      if (!user) return false
      const key = `liked${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`
      return user[key].includes(movieId)
    },
    [user],
  )

  const hasUserWatchedMovie = useCallback(
    (movieId) => {
      if (!user) return false
      const key = `watched${mediaType.get()[0].toUpperCase() + mediaType.get().substring(1)}s`
      return user[key].includes(movieId)
    },
    [user],
  )

  if (!user) return null

  for (let i = 0; i < Math.ceil(_rest.length / 4); i += 4) {
    rest.push(_rest.slice(i, i + 4))
  }

  const tileProps = {
    toggleLike,
    toggleWatch,
    hasUserLikedMovie,
    hasUserWatchedMovie,
  }

  return (
    <>
      <main style={{ margin: 16, overflow: 'hidden' }}>
        {showTopSections && (
          <>
            <S.ListHeader>Trending</S.ListHeader>
            {trending && (
              <S.SliderRow isTrending>
                <Slider {...sliderConfig} slidesToShow={3} slidesToScroll={1}>
                  {trending.map((movie) => (
                    <MovieTile isTrending key={movie.id} movie={movie} {...tileProps} />
                  ))}
                </Slider>
              </S.SliderRow>
            )}
            <S.ListHeader>Top</S.ListHeader>
            {top && (
              <S.SliderRow>
                <Slider {...sliderConfig} slidesToShow={3} slidesToScroll={3} rtl={true}>
                  {top.map((movie) => (
                    <MovieTile key={movie.id} movie={movie} {...tileProps} />
                  ))}
                </Slider>
              </S.SliderRow>
            )}
          </>
        )}
        <S.Search>
          <input
            type="text"
            value={movieQuery}
            placeholder="Search for ..."
            onChange={(e) => setMovieQuery(e.target.value)}
          />
        </S.Search>
        <S.ListHeader>More popular {MediaType.isMovie() ? 'movies' : 'shows'}</S.ListHeader>
        <div style={{ width: 1200, display: 'block', margin: '0 auto' }}>
          {!rest.length ? (
            <Flex direction="column" justify="center" align="center">
              <img height={300} src="/no-data.svg" />
              <h5>No movies found</h5>
            </Flex>
          ) : (
            rest.map((group) => (
              <div
                key={`group-${group[0].id}`}
                style={{ whiteSpace: 'nowrap', display: 'flex', justifyContent: 'space-around' }}
              >
                {group.map((movie) => (
                  <div key={movie.id} style={{ display: 'inline-block' }}>
                    <MovieTile movie={movie} {...tileProps} />
                  </div>
                ))}
              </div>
            ))
          )}
        </div>
      </main>
      <ScrollToTop />
    </>
  )
}

export default Movies
