import styled from 'styled-components'
import { colors } from '../../styles'

const FullScreen = styled.div`
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  max-width: 100%;
  max-height: 100%;
  position: absolute;
  overflow: hidden;
`

export const LoginPage = styled(FullScreen)`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: whitesmoke;
  /* background: linear-gradient(-30deg, #ffde03 50%, #0336ff 50%); */
`

export const LoginForm = styled.div`
  width: 500px;
  z-index: 999;
  background: white;
  padding: 16px 42px 42px 42px;
  border-radius: 42px;
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12),
    0 2px 4px -1px rgba(0, 0, 0, 0.2);

  h1 {
    color: #0336ff;
    font-weight: 900;
    font-size: 42px;
  }

  h3 {
    margin-top: 24px;
  }

  label {
    color: #aaa;
  }
`

export const LoginIlustration = styled.img`
  width: 180px;
  margin: 12px 0 -4px;
`

export const Particles = styled(FullScreen)``

export const LoginFields = styled.div`
  margin: 24px 0;
`

export const LoginField = styled.input`
  width: 100%;
  padding: 12px 0;
  display: block;
  box-sizing: border-box;
  font-family: 'Lato', sans-serif;
  margin: 16px 0;
  font-size: 14px;

  border: none;
  border-bottom: 1px solid rgba(0, 0, 0, 0.3);
`

export const Submit = styled.div`
  display: flex;
  padding: 16px 0;
  justify-content: center;
  align-items: center;

  button {
    border: none;
    padding: 10px 38px;
    margin: 24px 0 8px;
    font-size: 14px;
    border-radius: 24px;
    cursor: pointer;
    color: white;
    width: 100%;
    background: ${colors.PRIMARY};
    text-transform: uppercase;
  }
`

export const SocialLoginWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`

export const TwitterLoginButton = styled.div`
  content: ' ';
  width: 158px;
  height: 28px;
  background-image: url('https://cdn.cms-twdigitalassets.com/content/dam/developer-twitter/images/sign-in-with-twitter-gray.png');
`

export const FacebookLoginButton = styled.div`
  content: ' ';
  width: 158px;
  height: 28px;
  background-image: url('https://scontent-vie1-1.xx.fbcdn.net/v/t39.2365-6/17639236_1785253958471956_282550797298827264_n.png?_nc_cat=105&_nc_ohc=ZGeZmDvtbXcAQlbkKBMW1glQuPwNodS_SndAiVawSoZANbGUiHt2zXOwg&_nc_ht=scontent-vie1-1.xx&oh=bba29e09c9ef8f44acc692804a0eff64&oe=5E66C4EA');
  background-size: contain;
`

export const GoogleLoginButton = styled.div`
  content: ' ';
  width: 158px;
  height: 44px;
  background-image: url('https://developers.google.com/identity/images/btn_google_signin_light_normal_web.png');
  background-size: contain;
  background-repeat: no-repeat;
`
