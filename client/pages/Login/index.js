import React, { useEffect, useState } from 'react'
import 'particles.js'

import * as S from './styles'
import Flex from '../../components/Flex'

const PARTICLES_ROOT = 'js-particles-root'

function Login() {
  useEffect(() => window.particlesJS.load(PARTICLES_ROOT, '/particles.json'), [])
  const [isRegister, setIsRegister] = useState(false)

  return (
    <S.LoginPage>
      <S.Particles id={PARTICLES_ROOT} />
      <S.LoginForm>
        <Flex justify="center" align="center">
          <S.LoginIlustration src="/login-ilustration.svg" />
        </Flex>
        <h1>WatchThis!</h1>
        <h3>Login with email and password</h3>
        <form
          method="POST"
          style={{ padding: '0 16px' }}
          action={isRegister ? '/register' : '/login'}
        >
          <S.LoginFields>
            <S.LoginField name="email" autoComplete="off" placeholder="Email" />
            <S.LoginField
              name="password"
              autoComplete="off"
              placeholder="Password"
              type="password"
            />
            <label color="#77779f" htmlFor="register">
              Register
            </label>
            <input
              value={isRegister}
              onChange={() => setIsRegister(!isRegister)}
              type="checkbox"
              id="register"
            />
            <S.Submit>
              <button type="submit">{isRegister ? 'Register' : 'Login'}</button>
            </S.Submit>
          </S.LoginFields>
        </form>
        <h3>Use social login</h3>
        <S.SocialLoginWrapper>
          <a href="/twitter-login">
            <S.TwitterLoginButton />
          </a>
          <a href="/fb-login">
            <S.FacebookLoginButton />
          </a>
          <a href="/google-login">
            <S.GoogleLoginButton />
          </a>
        </S.SocialLoginWrapper>
      </S.LoginForm>
    </S.LoginPage>
  )
}

export default Login
