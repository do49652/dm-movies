import '@babel/polyfill'
import 'react-hot-loader'
import React from 'react'
import ReactDOM from 'react-dom'

import '../public/styles.scss'

import App from './App'

function renderApp() {
  ReactDOM.render(<App />, document.getElementById('app'))
}

renderApp()
if (module.hot) {
  module.hot.accept(renderApp)
}
