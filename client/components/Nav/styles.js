import styled from 'styled-components'
import { colors } from '../../styles'

export const Navbar = styled.nav`
  width: 100%;
  height: 80px;
  padding: 0 64px;
  box-sizing: border-box;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`

export const BrandTitle = styled.h3`
  margin: 0;
  font-weight: 900;
  a {
    color: ${colors.PRIMARY};
  }
`

export const NavMenu = styled.ul`
  margin: 0;
  display: flex;
  align-items: center;
  list-style: none;
  position: relative;

  li {
    margin: 0 8px;

    a {
      color: #aaa;
      font-weight: 700;
      text-transform: uppercase;
      transition: 0.2s opacity;
      transition: 0.3s color;
      font-size: 14px;

      &:hover {
        color: black;
      }
    }

    /* &::after {
      content: '';
      position: absolute;
      height: 2px;
      width: 100px;
      background: red;
      bottom: -10px;
      left: 0;
    } */
  }
`

export const ProfilePicture = styled.img`
  width: 36px;
  height: 36px;
  margin-left: 8px;
  border-radius: 50%;
`
