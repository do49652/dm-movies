import React, { useCallback } from 'react'
import { Link } from 'react-router-dom'

import * as S from './styles'
import Flex from '../Flex'

function Nav({ user }) {
  // const onChange = useCallback((ev) => {
  //   const movie = ev.target.value
  //   const search_params = new URLSearchParams(window.location.search)

  //   search_params.set('query', movie)
  //   // window.location.search = search_params.toString()
  // })

  // const movieQuery = new URLSearchParams(window.location.search).get('query')
  return (
    <S.Navbar>
      <Flex align="center" justify="space-between" style={{ height: '100%' }}>
        <S.BrandTitle>
          <Link to="/">WatchThis!</Link>
        </S.BrandTitle>
        <S.NavMenu>
          <li>
            <Link
              to="/"
              onClick={() => (localStorage.setItem('media', 'movie'), (location.href = '/'))}
            >
              Movies
            </Link>
          </li>
          <li>
            <Link
              to="/"
              onClick={() => (localStorage.setItem('media', 'show'), (location.href = '/'))}
            >
              Shows
            </Link>
          </li>
          <li>
            <Link to="/recommender">Recommender</Link>
          </li>
          <li>
            <a href="/logout">Logout</a>
          </li>
          <S.ProfilePicture src={user.photo} />
        </S.NavMenu>
      </Flex>
    </S.Navbar>
  )
}

export default Nav
