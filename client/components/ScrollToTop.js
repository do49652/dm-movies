import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import Flex from './Flex'

const OFFSET_LIMIT = 1100

const Button = styled(Flex)`
  width: 60px;
  height: 60px;
  right: 30px;
  bottom: 30px;
  position: fixed;
  border-radius: 50%;

  cursor: pointer;
  transition: 0.3s;
  transform: scale(${(p) => (p.isVisible ? 1 : 0)});
  border: 1px solid rgba(0, 0, 0, 0.2);
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12),
    0 2px 4px -1px rgba(0, 0, 0, 0.2);

  &:hover {
    box-shadow: none;
  }
`

function ScrollToTop() {
  const [isVisible, setVisible] = useState(false)

  // TODO: throttle this
  useEffect(() => {
    const onScroll = () => {
      if (window.pageYOffset > OFFSET_LIMIT && !isVisible) {
        setVisible(true)
      } else if (window.pageYOffset < OFFSET_LIMIT) setVisible(false)
    }

    window.addEventListener('scroll', onScroll)
    return () => window.removeEventListener('scroll', onScroll)
  })

  return (
    <Button
      onClick={() => window.scrollTo(0, 0)}
      isVisible={isVisible}
      justify="center"
      align="center"
    >
      <img src="/arrow.svg" />
    </Button>
  )
}

export default ScrollToTop
