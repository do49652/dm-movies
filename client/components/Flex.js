import styled from 'styled-components'

const Flex = styled.div`
  display: flex;
  ${(p) => (p.align ? `align-items: ${p.align}` : '')};
  ${(p) => (p.justify ? `justify-content: ${p.justify}` : '')};
  ${(p) => (p.direction ? `flex-direction: ${p.direction}` : '')};
`

export default Flex
