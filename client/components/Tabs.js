import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import { colors } from '../styles'

const Bar = styled.div`
  top: 59px;
  height: 4px;
  width: 200px;
  position: absolute;
  transition: 0.3s all;
  /* background: ${colors.PRIMARY}; */

  left: ${(p) => p.left}px;
  width: ${(p) => p.width}px;
`
const Header = styled.div`
  display: flex;
  padding: 8px 8px 0 8px;

  border-bottom: 1px solid #ddd;
`

const TabsContainer = styled.div`
  width: 100%;
  overflow: hidden;
  position: relative;
`

const Content = styled.div`
  width: 100%;
  padding-top: 16px;
  white-space: nowrap;
  /* transition: transform 0.3s; */
  transform: ${(p) => p.transform};

  & * {
    white-space: normal;
  }
`

const TabPanel = styled.div`
  width: 100%;
  vertical-align: top;
  display: inline-block;
  transition: 0.3s 0.3s all;
  opacity: ${(p) => (p.active ? 1 : 0)};
`

const TabTitle = styled.a`
  padding: 16px;
  cursor: pointer;
  font-weight: bold;
  transition: 0.5s all;
  text-transform: uppercase;
  color: ${(p) => (p.active ? p.activeColor : '#ccc')};
  font-family: 'IBM Plex Mono', monospace;
`

function Tab({ children }) {
  return children
}

function Tabs({ children, style, activeColor }) {
  const [activeIndex, setActiveIndex] = useState(0)
  const [barPosition, setBarPosition] = useState({ left: 0, width: 0 })
  const transform = `translateX(${-activeIndex * 100}%)`

  useEffect(() => {
    const tabHeaderPositionX = document.getElementById('js-tab-root').getBoundingClientRect().x
    const { x, width } = document
      .getElementById(`js-tab-header-${activeIndex}`)
      .getBoundingClientRect()

    setBarPosition({ left: x - tabHeaderPositionX + 15, width: width - 30 })
  }, [activeIndex])

  return (
    <TabsContainer style={style} id="js-tab-root">
      <Header style={{ marginLeft: -24 }}>
        {React.Children.map(children, (child, i) => {
          const onClick = () => setActiveIndex(i)
          return (
            <TabTitle
              id={`js-tab-header-${i}`}
              active={activeIndex === i}
              activeColor={activeColor}
              onClick={onClick}
            >
              {child.props.title}
            </TabTitle>
          )
        })}
      </Header>
      <Bar {...barPosition} style={{ background: activeColor }} />
      <Content transform={transform}>
        {React.Children.map(children, (child, i) => {
          const isActive = i === activeIndex
          const translateFactor = activeIndex - i
          return <TabPanel active={i === activeIndex}>{child.props.children}</TabPanel>
        })}
      </Content>
    </TabsContainer>
  )
}

Tabs.Tab = Tab

export default Tabs
