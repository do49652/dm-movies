import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import Login from './pages/Login'
import Nav from './components/Nav'
import Movies from './pages/Movies'
import Recommender from './pages/Recommender'
import Details from './pages/Details'
import Flex from './components/Flex'

export const LS_USER_KEY = 'dm:user'

function useAuthorization() {
  const [auth, setAuth] = useState(localStorage.getItem(LS_USER_KEY))

  const checkAuth = async () => {
    try {
      const res = await fetch('/authorized')
      const _auth = await res.json()
      setAuth(_auth)
      if (_auth.authorized) {
        localStorage.setItem(LS_USER_KEY, JSON.stringify(_auth))
      } else {
        localStorage.removeItem(LS_USER_KEY)
      }
    } catch (e) {
      localStorage.removeItem(LS_USER_KEY)
    }
  }

  useEffect(() => {
    checkAuth()
  }, [window.location.pathname])

  return { ...auth }
}

function App() {
  const { authorized, user } = useAuthorization()

  return (
    <Router>
      <div>
        {authorized && <Nav user={user} />}
        {authorized && (
          <Route path="/login">
            <Redirect to="/" />
          </Route>
        )}
        {authorized === false && (
          <Route path="/">
            <Redirect to="/login" />
          </Route>
        )}
        <Switch>
          <Route exact path="/">
            <Movies />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/details/:id">
            <Details />
          </Route>
          <Route path="/recommender">
            <Recommender />
          </Route>
          <Route path="*">
            <Flex justify="center" align="center">
              <img style={{ marginTop: '10%' }} width="40%" src="/404.svg" />
            </Flex>
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
