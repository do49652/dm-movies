class MediaType {
  get() {
    return localStorage.getItem('media') || 'movie'
  }
  isMovie() {
    this.get() === 'movie'
  }
}

export default new MediaType()
