import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import session from 'express-session';

import register from './routes/register';
import login from './routes/login';
import index from './routes/index';
import movies from './routes/movies';
import shows from './routes/shows';
import ratings from './routes/rating';
import recommender from './routes/recommender';

var app = express();
app.use(express.static('dist'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true,
  }),
);

app.use(passport.initialize());
app.use(passport.session());

app.use(register);
app.use(login);
app.use(movies);
app.use(shows);
app.use(ratings);
app.use(recommender);
app.use(index);

const port = process.env.PORT || 3000;
app.listen(port, console.log(`Server is actually at http://localhost:${port}`));

export default app;
