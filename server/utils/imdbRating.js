import fetch from 'node-fetch';

export default async (key) => {
  try {
    const html = await (await fetch(`https://www.imdb.com/title/${key}`)).text();
    const imdb = JSON.parse(`{${html.split('"aggregateRating": {')[1].split('}')[0]}}`);
    return { imdb_vote_average: imdb.ratingValue, imdb_vote_count: imdb.ratingCount };
  } catch {
    return {};
  }
};
