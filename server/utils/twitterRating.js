import fetch from 'node-fetch';

export default async (key) => {
  const html = await (await fetch(`https://twitter.com/${key}`)).text();
  const data = html.split('data-count=');

  let twitter_followers = 0;
  try {
    twitter_followers = parseInt(data[data.length - 2].split(' ')[0]);
  } catch {}
  return { twitter_followers };
};
