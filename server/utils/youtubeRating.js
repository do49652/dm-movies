import fetch from 'node-fetch';

export default async (key) => {
  const url = `https://www.youtube.com/watch?v=${key}`;
  const html = await (await fetch(url)).text();

  const views = html
    .split('watch-view-count')[1]
    .split('</div>')[0]
    .replace(/[^0-9]/g, '');

  let likes = html.split('like-button-renderer-like-button')[2].split('</span></button>')[0];
  likes = likes.substring(likes.lastIndexOf('>')).replace(/[^0-9]/g, '');

  let dislikes = html.split('like-button-renderer-dislike-button')[2].split('</span></button>')[0];
  dislikes = dislikes.substring(dislikes.lastIndexOf('>')).replace(/[^0-9]/g, '');

  let youtube_vote_average = null;
  let youtube_views_count = null;

  try {
    youtube_views_count = parseInt(views);
  } catch {}

  try {
    youtube_vote_average =
      parseInt(Math.round((parseInt(likes) / (parseInt(likes) + parseInt(dislikes))) * 100)) /
      100.0;
  } catch {}

  return { youtube_views_count, youtube_vote_average };
};
