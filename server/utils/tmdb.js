import fetch from 'node-fetch';

const url = (mediatype) => `https://api.themoviedb.org/3/${mediatype}/`;
const append_to_response =
  '&append_to_response=images,videos,credits,similar_movies,external_ids';
const api_key = '0e711403ae58a41ddf7ca401d86ac4c0';

const api_url = (id, mediatype) => `${url(mediatype)}${id}?api_key=${api_key}&language=en${append_to_response}`;

export default async (id, mediatype = 'movie') => {
  const res = await fetch(api_url(id, mediatype));
  const movieobject = await res.json();

  const movie = {
    id,
    popularity: movieobject.popularity,
    belongs_to_collection: mediatype === 'movie' && movieobject.belongs_to_collection
      ? movieobject.belongs_to_collection.id
      : null,
    external_ids: movieobject.external_ids,
    original_title: movieobject[mediatype === 'movie' ? 'original_title' : 'original_name'],
    overview: movieobject.overview,
    release_date: movieobject.release_date,
    runtime: movieobject.runtime,
    status: movieobject.status,
    title: movieobject[mediatype === 'movie' ? 'title' : 'name'],
    vote_average: movieobject.vote_average,
    vote_count: movieobject.vote_count,
    images_backdrops: movieobject.images.backdrops.map((img) => img.file_path),
    images_posters: movieobject.images.posters.map((img) => img.file_path),
    videos: movieobject.videos.results.filter((v) => v.key).map((v) => v.key),
    similar_movies: mediatype === 'movie' ? movieobject.similar_movies.results.map((m) => m.id) : null,
    similar: mediatype === 'tv' && movieobject.similar ? movieobject.similar.results.map((m) => m.id) : null,
  };

  return movie;
};
