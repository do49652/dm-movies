import mongoose, { Schema } from 'mongoose'
import { dbUrl } from '.'

const User = mongoose.model(
  'User',
  new Schema({
    // LOCAL STRATEGY INFO
    email: String,
    password: String,
    // SHARED SOCIAL LOGIN INFO
    name: String,
    photo: String,
    profileId: Number,
    strategy: String,
    // FB LOGIN INFO
    fbId: String,
    fbEmail: String,
    fbAccessToken: String,
    fbRefreshToken: String,
    // GOOGLE LOGIN INFO
    googleAccessToken: String,
    googleRefreshToken: String,
    // USER PROFILE RELATED INFO
    likedMovies: Array, // movie ids
    watchedMovies: Array, // movie ids
    likedShows: Array, // show ids
    watchedShows: Array, // show ids
  }),
)

export const findUser = (query) =>
  new Promise((resolve) => {
    User.findOne(query, (err, user) => resolve(user))
  })

export const createUser = (user) =>
  new Promise((resolve, reject) => {
    User.find({ profileId: user.profileId }, (err, users) => {
      if (err) {
        reject(err)
        return
      }
      if (!users || !users.length) {
        const newUser = new User({ ...user })
        newUser.save()
        resolve(newUser)
      }
    })
  })

export const updateUser = (user) =>
  new Promise((resolve) => {
    User.findOne({ profileId: user.profileId }, (err, _user) => {
      if (_user) {
        _user.password = user.password || _user.password
        _user.fbId = user.fbId || _user.fbId
        _user.fbEmail = user.fbEmail || _user.fbEmail
        _user.fbAccessToken = user.fbAccessToken || _user.fbAccessToken
        _user.fbRefreshToken = user.fbRefreshToken || _user.fbRefreshToken
        _user.googleAccessToken = user.googleAccessToken || _user.googleAccessToken
        _user.googleRefreshToken = user.googleRefreshToken || _user.googleRefreshToken
        _user.likedMovies = user.likedMovies || _user.likedMovies
        _user.watchedMovies = user.watchedMovies || _user.watchedMovies
        _user.likedShows = user.likedShows || _user.likedShows
        _user.watchedShows = user.watchedShows || _user.watchedShows
        _user.save(null)
        resolve(_user)
      }
    })
  })
