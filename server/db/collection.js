import mongoose, { Schema } from 'mongoose';

const BelongsToCollection = mongoose.model(
  'BelongsToCollection',
  new Schema({
    id: Number, // collection id
    name: String,
    poster_path: String,
    backdrop_path: String,
  }),
);
