import mongoose, { Schema } from 'mongoose';

const Crew = mongoose.model(
  'Crew',
  new Schema({
    id: Number, // crew id
    name: String,
    gender: Number,
    job: String,
    profile_path: String,
  }),
);
