import mongoose, { Schema } from 'mongoose';

export const Cast = mongoose.model(
  'Cast',
  new Schema({
    id: Number, // cast id
    name: String,
    gender: Number,
    profile_path: String,
  }),
);
