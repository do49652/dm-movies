import mongoose, { Schema } from 'mongoose';
import { Cast } from './cast';

export const TVShow = mongoose.model(
  'TVShow',
  new Schema({
    id: Number, // movie id
    genres: Array, // strings
    homepage: String,
    external_ids: Array,
    original_language: String,
    original_title: String,
    overview: String,
    popularity: Number,
    release_date: String,
    title: String,
    vote_average: Number,
    vote_count: Number,
    imdb_vote_average: Number,
    imdb_vote_count: Number,
    images_backdrops: Array, // paths
    images_posters: Array, // paths
    videos: Array, // youtube ids
    similar: Array, // movie ids
    cast: Array, // { cast id, character }
    crew: Array // { crew id, department }
  })
);

export const findShow = async (_show) => {
  const show = await TVShow.findOne(_show).exec();
  const cast = await Cast.find({
    id: { $in: show.cast.map((c) => c.id) },
  }).exec();

  show.cast = show.cast.map((c) => {
    const cc = cast.find((cc) => cc.id === c.id);
    return {
      id: c.id,
      character: c.character,
      name: cc.name,
      gender: cc.gender,
      profile_path: cc.profile_path,
    };
  });
  return show;
};

export const findShows = async (query) => {
  const shows = await TVShow.find(query).exec();
  return shows;
};

export const find = async (query, skip, limit) => {
  const shows = await TVShow.find(!query ? null : { $text: { $search: query } })
    .sort({ popularity: -1 })
    .skip(skip || 0)
    .limit(limit || 20)
    .exec();
  return shows;
};

export const update = async (_show) => {
  const show = await TVShow.findOne({ id: _show.id }).exec();
  const s = { ...show._doc, ..._show };
  await TVShow.updateOne({ id: _show.id }, s).exec();
  return s;
};

export const getSimilarShows = async (ids) => {
  let showIds = null;
  
  if (ids.length === 1) {
    const sho = await TVShow.findOne({ id: parseInt(ids[0], 10) }).exec();
    showIds = JSON.parse(JSON.stringify(sho.similar.map((s) => ({ id: ids[0], show: s })))); // ¯\_('')_/¯
  } else {
    showIds = (
      await TVShow.mapReduce({
        map: `function() {
        if (this.similar && [${ids.join(',')}].includes(this.id)) {
          emit(this.id, this.similar);
        }
      }`,
        reduce: `function(key, values) {
        return values;
      }`,
        resolveToObject: true,
      })
    ).results.reduce((a, c) => a.concat(c.value.map((s) => ({ id: c._id, show: s }))), []);
  }

  return showIds;
};

export const getSimilarMostPopularShows = async (ids, skip, limit) => {
  let showIds = null;
  
  if (ids.length === 1) {
    const sho = await TVShow.findOne({ id: parseInt(ids[0], 10) }).exec();
    showIds = sho.similar;
  } else {
    showIds = (
      await TVShow.mapReduce({
        map: `function() {
        if (this.similar && [${ids.join(',')}].includes(this.id)) {
          emit(this.id, this.similar);
        }
      }`,
        reduce: `function(key, values) {
        return values;
      }`,
        resolveToObject: true,
      })
    ).results.reduce((a, c) => a.concat(c.value), []);
  }

  var showsPopularity = (await TVShow.find({ id: { $in: showIds } }, { _id: 0, id: 1 })
    .sort({ popularity: -1 })
    .skip(skip)
    .limit(limit)
    .exec())
    .map(({ id }) => id);

  return showsPopularity;
}
