import mongoose, { Schema } from 'mongoose';

const Company = mongoose.model(
  'Company',
  new Schema({
    id: Number, // company id
    name: String,
    logo_path: String,
  }),
);
