import mongoose, { Schema } from 'mongoose';
import { Cast } from './cast';

export const Movie = mongoose.model(
  'Movie',
  new Schema({
    id: Number, // movie id
    belongs_to_collection: Number, // collection id
    budget: Number,
    genres: Array, // strings
    homepage: String,
    external_ids: Array,
    original_language: String,
    original_title: String,
    overview: String,
    popularity: Number,
    production_companies: Array, // company ids
    release_date: String,
    revenue: Number,
    runtime: Number,
    status: String,
    tagline: String,
    title: String,
    vote_average: Number,
    vote_count: Number,
    imdb_vote_average: Number,
    imdb_vote_count: Number,
    youtube_vote_average: Number,
    youtube_views_count: Number,
    twitter_followers: Number,
    metascore: Number,
    rottenTomatoes: Number,
    images_backdrops: Array, // paths
    images_posters: Array, // paths
    videos: Array, // youtube ids
    keywords: Array, //strings
    similar_movies: Array, // movie ids
    cast: Array, // { cast id, character }
    crew: Array, // { crew id, department }
  }),
);

export const findMovie = async (_movie) => {
  const movie = await Movie.findOne(_movie).exec();
  const cast = await Cast.find({
    id: { $in: movie.cast.map((c) => c.id) },
  }).exec();

  movie.cast = movie.cast.map((c) => {
    const cc = cast.find((cc) => cc.id === c.id);
    return {
      id: c.id,
      character: c.character,
      name: cc.name,
      gender: cc.gender,
      profile_path: cc.profile_path,
    };
  });
  return movie;
};

export const findMovies = async (query) => {
  const movies = await Movie.find(query).exec();
  return movies;
};

export const find = async (query, skip, limit) => {
  const movies = await Movie.find(!query ? null : { $text: { $search: query } })
    .sort({ popularity: -1 })
    .skip(skip || 0)
    .limit(limit || 20)
    .exec();
  return movies;
};

export const update = async (_movie) => {
  const movie = await Movie.findOne({ id: _movie.id }).exec();
  const m = { ...movie._doc, ..._movie };
  await Movie.updateOne({ id: _movie.id }, m).exec();
  return m;
};

export const getSimilarMovies = async (ids) => {
  let movieIds = null;
  
  if (ids.length === 1) {
    const mov = await Movie.findOne({ id: parseInt(ids[0], 10) }).exec();
    movieIds = JSON.parse(JSON.stringify(mov.similar_movies.map((m) => ({ id: ids[0], movie: m })))); // ¯\_('')_/¯
  } else {
    movieIds = (
      await Movie.mapReduce({
        map: `function() {
        if (this.similar_movies && [${ids.join(',')}].includes(this.id)) {
          emit(this.id, this.similar_movies);
        }
      }`,
        reduce: `function(key, values) {
        return values;
      }`,
        resolveToObject: true,
      })
    ).results.reduce((a, c) => a.concat(c.value.map((m) => ({ id: c._id, movie: m }))), []);
  }

  return movieIds;
};

export const getSimilarMostPopularMovies = async (ids, skip, limit) => {
  let movieIds = null;
  
  if (ids.length === 1) {
    const mov = await Movie.findOne({ id: parseInt(ids[0], 10) }).exec();
    movieIds = mov.similar_movies;
  } else {
    movieIds = (
      await Movie.mapReduce({
        map: `function() {
        if (this.similar_movies && [${ids.join(',')}].includes(this.id)) {
          emit(this.id, this.similar_movies);
        }
      }`,
        reduce: `function(key, values) {
        return values;
      }`,
        resolveToObject: true,
      })
    ).results.reduce((a, c) => a.concat(c.value), []);
  }

  var moviesPopularity = (await Movie.find({ id: { $in: movieIds } }, { _id: 0, id: 1 })
    .sort({ popularity: -1 })
    .skip(skip)
    .limit(limit)
    .exec())
    .map(({ id }) => id);

  return moviesPopularity;
}
