import express from 'express'
import fetch from 'node-fetch'

import youtubeRating from '../utils/youtubeRating'
import twitterRating from '../utils/twitterRating'
import imdbRating from '../utils/imdbRating'

const router = express.Router()

router.get('/youtube/:key', async (req, res) => {
  const { key } = req.params
  const yt = await youtubeRating(key)
  res.set('Cache-Control', 'public, max-age=86400')
  res.json(yt)
})

router.get('/ratings/:imdbId', async (req, res) => {
  const { imdbId } = req.params
  const url = `http://www.omdbapi.com/?i=${imdbId}&apikey=a38ca939`
  const { Metascore, Ratings } = await (await fetch(url)).json()
  const rottenTomatoes = Ratings.find((r) => r.Source.toLowerCase().includes('rotten'))?.Value

  const imdb = await imdbRating(imdbId)

  // CACHE THIS RESPONSE FOR A DAY
  res.set('Cache-Control', 'public, max-age=86400')

  res.json({
    metascore: Metascore,
    rottenTomatoes,
    ...imdb,
  })
})

router.get('/tmdb/:id', async (req, res) => {
  const { id } = req.params
  const url = `https://api.themoviedb.org/3/movie/${id}?api_key=0e711403ae58a41ddf7ca401d86ac4c0&language=en-US`
  const { vote_average, vote_count } = await (await fetch(url)).json()
  res.set('Cache-Control', 'public, max-age=86400')
  res.json({ vote_average, vote_count })
})

router.get('/twitter/:key', async (req, res) => {
  const { key } = req.params
  res.set('Cache-Control', 'public, max-age=86400')
  res.json(await twitterRating(key))
})

export default router
