import { resolve } from 'path'
import express from 'express'
import { updateUser } from '../db/user'
const router = express.Router()

router.get('/authorized', (req, res) =>
  res.json({ authorized: req.isAuthenticated(), user: req.user }),
)

router.get('/privacy', (req, res) => res.redirect('/privacy.html'))

router.post('/user', async (req, res) => {
  const { email, likedMovies, watchedMovies, likedShows, watchedShows } = req.body
  const user = await updateUser({
    profileId: req.user.profileId,
    likedMovies,
    watchedMovies,
    likedShows,
    watchedShows,
  })
  res.json({ user })
})

// FIX for browser routing refresh
router.get('*', (req, res) => res.sendFile(resolve('dist', 'index.html')))

export default router
