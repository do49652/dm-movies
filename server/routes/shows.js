/* eslint-disable indent */

import express from 'express';
import fetch from 'node-fetch';
import { find, findShow, findShows, TVShow, update } from '../db/show';
import tmdb from '../utils/tmdb';

const router = express.Router();

router.get('/shows', async (req, res) => {
  const { skip, limit, query } = req.query;

  try {
    res.json(await find(query, skip ? parseInt(skip) : null, limit ? parseInt(limit) : null));
  } catch {
    res.json(await find(query));
  }
});

router.post('/shows', async (req, res) => {
  const { ids } = req.body;
  res.json(await findShows({ id: { $in: ids } }));
});

router.get('/show/:id', async (req, res) => {
  const { id } = req.params;
  res.json(await findShow({ id }));
});

router.get('/shows/:id/update', async (req, res) => {
  const { id } = req.params;
  const show = await tmdb(id, 'tv');
  update(show);
  res.json(show);
});

router.post('/shows/:id/updateRatings', async (req, res) => {
  const { id } = req.params;
  const { body } = req;

  for (const key of Object.keys(body)) {
    try {
      if (body[key]) body[key] = parseFloat(body[key].replace(/[^0-9,.]/g, ''));
    } catch {}
  }

  update({ id, ...req.body });
  res.json({});
});

/*
router.get('/shows/facebook', async (req, res) => {
  const fbRes = await fetch(
    `https://graph.facebook.com/v4.0/me/shows?fields=name%2Cid%2Cfan_count&access_token=${req.user.fbAccessToken}`,
  );

  const { data } = await fbRes.json();

  const showsIds = (
    await TVShows.mapReduce({
      map: `function() {
      if (
        (
          this.external_ids
          && this.external_ids[0]
          && this.external_ids[0].facebook_id
          && [${data.map(({ name }) => `"${name.replace(/ /g, '').toLowerCase()}"`).join(', ')}]
            .includes(this.external_ids[0].facebook_id.replace(/ /g, '').toLowerCase())
        )
        || (
          [${data.map(({ name }) => `"${name.replace(/ /g, '').toLowerCase()}"`).join(', ')}]
            .includes(this.title.replace(/ /g, '').toLowerCase())
        )
      ) {
        emit(this.id, (this.external_ids[0].facebook_id || this.title).replace(/ /g, '').toLowerCase());
      }
    }`,
      reduce: `function(key, values) {
      return values[0];
    }`,
      resolveToObject: true,
    })
  ).results.map(({ _id }) => _id);

  res.json(await findShows({ id: { $in: showsIds } }));
});
*/

export default router;
