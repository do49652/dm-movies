import express from 'express'
import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as FacebookStrategy } from 'passport-facebook'
import { Strategy as TwitterStrategy } from 'passport-twitter'
import { Strategy as GoogleStrategy } from 'passport-google-oauth20'

import { findUser, createUser, updateUser } from '../db/user'

const TWITTER_API_KEY = '5QIerWBJQwIueAOviOz1u6OTP'
const TWITTER_SECRET_KEY = 'uhAAjdX7ZMGrMQmJMLcVSVXub45aBDQPm8RDIeE1mldehNqBSK'

const router = express.Router()

passport.use(
  'local-login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    async (email, password, done) => {
      const user = await findUser({ email })

      if (!user) {
        return done(null, false, {
          message: 'Unknown User',
        })
      }

      if (password !== user.password) {
        return done(null, false, {
          message: 'Invalid password',
        })
      }

      return done(null, user.profileId)
    },
  ),
)

passport.use(
  'facebook-login',
  new FacebookStrategy(
    {
      clientID: '248009122783304',
      clientSecret: '92a75f8a3e80cbbf708e63bc1a04c7fa',
      profileFields: ['id', 'email'],
    },
    async (accessToken, refreshToken, profile, done) => {
      const email = profile.emails[0].value
      const profileId = Number(profile.id)
      const user = await findUser({ profileId })
      if (!user) {
        await createUser({
          email,
          profileId,
          name: profile.username,
          fbAccessToken: accessToken,
          fbRefreshToken: refreshToken,
          photo: null, // TODO: Revert this
          strategy: 'facebook',
        })
      } else {
        await updateUser({
          profileId,
          fbAccessToken: accessToken,
          fbRefreshToken: refreshToken,
        })
      }

      return done(null, profileId)
    },
  ),
)

passport.use(
  'twitter-login',
  new TwitterStrategy(
    {
      consumerKey: TWITTER_API_KEY,
      consumerSecret: TWITTER_SECRET_KEY,
      // TODO: add protocol and host here
      callbackURL: 'http://localhost:3000/twitter-login/callback',
    },
    async (token, _tokenSecret, profile, done) => {
      const profileId = Number(profile.id)
      const record = await findUser({ profileId })
      if (!record) {
        try {
          await createUser({
            profileId,
            twitterToken: token,
            name: profile.username,
            photo: profile.photos[0].value,
            strategy: 'twitter',
          })
          done(null, profile.id)
        } catch (e) {
          console.error(e)
        }
      } else {
        done(null, profile.id)
      }
    },
  ),
)

passport.use(
  'google-login',
  new GoogleStrategy(
    {
      clientID: '604486240911-ger5h7v4u8nhj8sn9oop1u5opiqhjksa.apps.googleusercontent.com',
      clientSecret: 'W8Y0v9_Tb14zVx4oF1eN-KCu',
    },
    async (accessToken, refreshToken, profile, done) => {
      const email = profile.emails[0].value
      const profileId = Number(profile.id)
      const user = await findUser({ profileId })
      if (!user) {
        await createUser({
          email,
          profileId,
          name: profile.displayName,
          googleAccessToken: accessToken,
          googleRefreshToken: refreshToken,
          photo: profile.photos[0].value,
          strategy: 'google',
        })
      } else {
        await updateUser({
          profileId,
          googleAccessToken: accessToken,
          googleRefreshToken: refreshToken,
        })
      }

      return done(null, profileId)
    },
  ),
)

passport.serializeUser((profileId, done) => {
  done(null, Number(profileId))
})

passport.deserializeUser(async (profileId, done) => {
  done(null, await findUser({ profileId: Number(profileId) }))
})

router.post(
  '/login',
  passport.authenticate('local-login', {
    successRedirect: '/',
    failureRedirect: '/',
  }),
)

const fbAuth = (req, res, next) => {
  const host = req.get('host')
  const isLocal = host.includes('localhost')
  const protocol = isLocal ? 'http://' : 'https://'

  return passport.authenticate('facebook-login', {
    successRedirect: '/',
    failureRedirect: '/',
    scope: ['email', 'user_likes'],
    callbackURL: `${protocol}${host}/fb-login/callback`,
  })(req, res, next)
}

router.get('/fb-login', fbAuth)
router.get('/fb-login/callback', fbAuth)

router.get('/twitter-login', passport.authenticate('twitter-login'))
router.get(
  '/twitter-login/callback',
  passport.authenticate('twitter-login', {
    successFlash: true,
    successRedirect: '/',
    failureRedirect: '/',
  }),
)

const googleAuth = (req, res, next) => {
  const host = req.get('host')
  const isLocal = host.includes('localhost')
  const protocol = isLocal ? 'http://' : 'https://'

  return passport.authenticate('google-login', {
    successRedirect: '/',
    failureRedirect: '/',
    scope: ['profile', 'email'],
    callbackURL: `${protocol}${host}/google-login/callback`,
  })(req, res, next)
}

router.get('/google-login', googleAuth)
router.get('/google-login/callback', googleAuth)

router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

export default router
