import express from 'express';
import { findMovies, getSimilarMovies, getSimilarMostPopularMovies } from '../db/movie';
import { getSimilarMostPopularShows, getSimilarShows, findShows } from '../db/show';

const router = express.Router();

const cache = {};

router.get('/recommend-movies', async (req, res) => {
  if (!req.user) return res.status(401).send();
  try {
    const { watchedMovies, likedMovies } = req.user;

    const key = `m${likedMovies.sort()}`;
    let similar = null;

    if (cache[key]) similar = cache[key];
    else {
      similar = (await getSimilarMovies(likedMovies))
        .filter(({ movie }) => !watchedMovies.includes(movie));
      cache[key] = similar;
    }
    
    const { skip, limit } = req.query;
    const movies = similar;//.slice(skip, skip + limit);

    const likedMovieDetails = await findMovies({ id: { $in: [...new Set(similar.map(({ id }) => id))] } });
    const movieDetails = await findMovies({ id: { $in: movies.map(({ movie }) => movie) } });
    
    res.json({
      likedMovieDetails,
      recommendations: movies.map(({ id, movie }) => ({ id, movie: movieDetails.find(m => m.id === movie) })),
    });
  } catch (err) {
    res.json([]);
  }
});

router.get('/recommend-popular-movies', async (req, res) => {
  if (!req.user) return res.status(401).send();
  try {
    const { skip, limit } = req.query;
    const { watchedMovies, likedMovies } = req.user;
    const similar = (await getSimilarMostPopularMovies(likedMovies, parseInt(skip), parseInt(limit)))
      .filter(({ movie }) => !watchedMovies.includes(movie));
    
    res.json(await findMovies({ id: { $in: similar } }));
  } catch (err) {
    res.json([]);
  }
});

router.get('/recommend-movies/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const similar = await getSimilarMovies([id]);
    res.json(similar);
  } catch (err) {
    res.json([]);
  }
});

router.get('/recommend-shows', async (req, res) => {
  if (!req.user) return res.status(401).send();
  try {
    const { watchedShows: watchedMovies, likedShows: likedMovies } = req.user;

    const key = `s${likedMovies.sort()}`;
    let similar = null;

    if (cache[key]) similar = cache[key];
    else {
      similar = (await getSimilarShows(likedMovies))
        .filter(({ show }) => !watchedMovies.includes(show));
      cache[key] = similar;
    }
    
    const { skip, limit } = req.query;
    const shows = similar;//.slice(skip, skip + limit);

    const likedShowDetails = await findShows({ id: { $in: [...new Set(similar.map(({ id }) => id))] } });
    const showDetails = await findShows({ id: { $in: shows.map(({ show }) => show) } });
    
    res.json({
      likedShowDetails,
      recommendations: shows.map(({ id, show }) => ({ id, show: showDetails.find(s => s.id === show) })),
    });
  } catch (err) {
    res.json([]);
  }
});

router.get('/recommend-popular-shows', async (req, res) => {
  if (!req.user) return res.status(401).send();
  try {
    const { skip, limit } = req.query;
    const { watchedShows: watchedMovies, likedShows: likedMovies } = req.user;
    const similar = (await getSimilarMostPopularShows(likedMovies, parseInt(skip), parseInt(limit)))
      .filter(({ show }) => !watchedMovies.includes(show));
    
    res.json(await findShows({ id: { $in: similar } }));
  } catch (err) {
    res.json([]);
  }
});

router.get('/recommend-shows/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const similar = await getSimilarShows([id]);
    res.json(similar);
  } catch (err) {
    res.json([]);
  }
});

export default router;
