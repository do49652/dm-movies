import express from 'express';
import { createUser } from '../db/user';
const router = express.Router();

router.post('/register', async (req, res) => {
  const { email, password } = req.body;
  await createUser({ email, password, profileId: Math.random() }); // ik
  res.redirect('/');
});

export default router;
