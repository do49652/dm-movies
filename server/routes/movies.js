/* eslint-disable indent */

import express from 'express'
import fetch from 'node-fetch'
const googleTrends = require('google-trends-api')

import { find, findMovie, findMovies, Movie, update } from '../db/movie'
import tmdb from '../utils/tmdb'

const GOOGLE_MOVIE_CATEGORY = 34

const router = express.Router()

router.get('/movies', async (req, res) => {
  const { skip, limit, query } = req.query

  try {
    res.json(await find(query, skip ? parseInt(skip) : null, limit ? parseInt(limit) : null))
  } catch {
    res.json(await find(query))
  }
})

router.post('/movies', async (req, res) => {
  const { ids } = req.body
  res.json(await findMovies({ id: { $in: ids } }))
})

router.get('/movie/:id', async (req, res) => {
  const { id } = req.params
  res.json(await findMovie({ id }))
})

router.get('/trends/', async (req, res) => {
  const { keyword } = req.query
  const r = await googleTrends.interestOverTime({
    keyword,
    category: GOOGLE_MOVIE_CATEGORY,
  })

  return res.json(JSON.parse(r).default.timelineData)
})

router.get('/movies/:id/update', async (req, res) => {
  const { id } = req.params
  const movie = await tmdb(id)
  update(movie)
  res.json(movie)
})

router.post('/movies/:id/updateRatings', async (req, res) => {
  const { id } = req.params
  const { body } = req

  for (const key of Object.keys(body)) {
    try {
      if (body[key]) body[key] = parseFloat(body[key].replace(/[^0-9,.]/g, ''))
    } catch {}
  }

  update({ id, ...req.body })
  res.json({})
})

router.get('/movies/facebook', async (req, res) => {
  const fbRes = await fetch(
    `https://graph.facebook.com/v4.0/me/movies?fields=name%2Cid%2Cfan_count&access_token=${req.user.fbAccessToken}`,
  )

  const { data } = await fbRes.json()

  const movieIds = (
    await Movie.mapReduce({
      map: `function() {
      if (
        (
          this.external_ids
          && this.external_ids[0]
          && this.external_ids[0].facebook_id
          && [${data.map(({ name }) => `"${name.replace(/ /g, '').toLowerCase()}"`).join(', ')}]
            .includes(this.external_ids[0].facebook_id.replace(/ /g, '').toLowerCase())
        )
        || (
          [${data.map(({ name }) => `"${name.replace(/ /g, '').toLowerCase()}"`).join(', ')}]
            .includes(this.title.replace(/ /g, '').toLowerCase())
        )
      ) {
        emit(this.id, (this.external_ids[0].facebook_id || this.title).replace(/ /g, '').toLowerCase());
      }
    }`,
      reduce: `function(key, values) {
      return values[0];
    }`,
      resolveToObject: true,
    })
  ).results.map(({ _id }) => _id)

  res.json(await findMovies({ id: { $in: movieIds } }))
})

export default router
